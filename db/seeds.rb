# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

u1 = User.create_with(first_name: 'Jorge', last_name: 'Martinez', age: 32, location: 'Santiago').find_or_create_by(email: 'test@test.com')
u2 = User.create_with(first_name: 'Camilo', last_name: 'Salazar', age: 26, location: 'Valparaiso').find_or_create_by(email: 'test2@test.com')

p1 = Post.create(user: u1, title: 'Manejo financiero de un proyecto de software', content: 'Introduccion: los proyectos de software...')
p2 = Post.create(user: u1, title: 'Instalación de GraphQL en Rails', content: 'Hola! hoy instalaremos...')
p3 = Post.create(user: u2, title: 'Ionic y Android Marshmallow', content: 'Al utilizar Ionic con las ultimas versiones...')
p4 = Post.create(user: u2, title: 'Como utilizar distintos indices en PostgreSQL y Rails', content: 'Introduccion: los indices tienen distinta utilidad...')

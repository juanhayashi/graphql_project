# app/graphql/types/user_type.rb
Types::UserType = GraphQL::ObjectType.define do
  name 'User'

  field :id, !types.ID
  field :first_name, !types.String
  field :last_name, !types.String
  field :email, !types.String
  field :age, types.Int
  field :location, types.String

  field :posts, !types[Types::PostType]
end
